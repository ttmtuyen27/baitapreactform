import logo from "./logo.svg";
import "./App.css";
import BaiTapReactForm from "./BaiTapReactForm/BaiTapReactForm";

function App() {
  return (
    <div className="App">
      <BaiTapReactForm />
    </div>
  );
}

export default App;
