import { connect } from "react-redux";

export class ValidatorForm {
  kiemTraRong = (value, idErr, message) => {
    if (!value) {
      document.getElementById(idErr).innerText = message;
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  };
  kiemTraMaSvHopLe = (value, danhSachSinhVien) => {
    let index = danhSachSinhVien.findIndex((sv) => {
      return value == sv.maSv;
    });
    console.log(index);
    console.log("ds", danhSachSinhVien);
    if (index != -1) {
      document.getElementById("tbMaSv").innerText = "Mã sinh viên đã tồn tại";
      return false;
    } else {
      document.getElementById("tbMaSv").innerText = "";
      return true;
    }
  };
  kiemTraEmailHopLe = (value) => {
    let pattern =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!pattern.test(value)) {
      document.getElementById("tbEmail").innerText = "Email không hợp lệ";
      return false;
    } else {
      document.getElementById("tbEmail").innerText = "";
      return true;
    }
  };
  kiemTraTenHopLe = (value) => {
    let pattern = /^[a-z ,.'-]+$/i;
    if (!pattern.test(value)) {
      document.getElementById("tbTen").innerText = "Tên không hợp lệ";
      return false;
    } else {
      document.getElementById("tbTen").innerText = "";
      return true;
    }
  };
  kiemTraSdt = (value) => {
    let pattern = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    if (!pattern.test(value)) {
      document.getElementById("tbSdt").innerText = "Số điện thoại không hợp lệ";
      return false;
    } else {
      document.getElementById("tbSdt").innerText = "";
      return true;
    }
  };
  kiemTraMaSvEdit = (value, list) => {
    let index = list.findIndex((sv) => {
      return sv.maSv == index;
    });
  };
}
