export const CHANGE_FORM = "CHANGE_FORM";
export const SUBMIT = "SUBMIT";
export const DELETE = "DELETE";
export const EDIT = "EDIT";
export const CHANGE_SEARCH_FORM = "CHANGE_SEARCH_FORM";
export const SEARCH = "SEARCH";
