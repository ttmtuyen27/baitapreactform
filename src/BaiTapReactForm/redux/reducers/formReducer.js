import { ValidatorForm } from "../../validation/Validation";
import {
  CHANGE_FORM,
  CHANGE_SEARCH_FORM,
  DELETE,
  EDIT,
  SEARCH,
  SUBMIT,
} from "../constants/constants";

let initialState = {
  infor: {
    maSv: "",
    ten: "",
    sdt: "",
    email: "",
  },
  danhSachSinhVien: [],
  editSv: null,
  isDisabled: false,
  search: "",
  seachList: [],
};

let validator = new ValidatorForm();

export const formReducers = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_FORM: {
      let name = payload.target.name;
      let value = payload.target.value;
      state.infor = { ...state.infor, [name]: value };
      return { ...state };
    }
    case SUBMIT: {
      let cloneDanhSachSinhVien = [...state.danhSachSinhVien];
      let isValid;
      if (state.editSv == null) {
        let isValidMaSv =
          validator.kiemTraRong(
            state.infor.maSv,
            "tbMaSv",
            "Mã sinh viên không được để rỗng"
          ) &&
          validator.kiemTraMaSvHopLe(state.infor.maSv, state.danhSachSinhVien);
        let isValidTen =
          validator.kiemTraRong(
            state.infor.ten,
            "tbTen",
            "Tên sinh viên không được để rỗng"
          ) && validator.kiemTraTenHopLe(state.infor.ten);
        let isValidSdt =
          validator.kiemTraRong(
            state.infor.sdt,
            "tbSdt",
            "Số điện thoại không được để rỗng"
          ) && validator.kiemTraSdt(state.infor.sdt);
        let isValidEmail =
          validator.kiemTraRong(
            state.infor.email,
            "tbEmail",
            "Email không được để rỗng"
          ) && validator.kiemTraEmailHopLe(state.infor.email);
        isValid = isValidMaSv && isValidTen && isValidSdt && isValidEmail;
        if (isValid) {
          cloneDanhSachSinhVien.push(state.infor);
          let cloneInfor = { ...state.infor };
          cloneInfor.maSv = "";
          cloneInfor.ten = "";
          cloneInfor.sdt = "";
          cloneInfor.email = "";
          state.infor = cloneInfor;
        }
      } else {
        let index = cloneDanhSachSinhVien.findIndex((item) => {
          return item.maSv == state.editSv.maSv;
        });
        cloneDanhSachSinhVien[index] = state.infor;
        state.isDisabled = false;
        let cloneInfor = { ...state.infor };
        cloneInfor.maSv = "";
        cloneInfor.ten = "";
        cloneInfor.sdt = "";
        cloneInfor.email = "";
        state.infor = cloneInfor;
      }
      state.danhSachSinhVien = cloneDanhSachSinhVien;
      state.editSv = null;
      state.seachList = [];
      return { ...state };
    }
    case DELETE: {
      let cloneDanhSachSinhVien = [...state.danhSachSinhVien];
      let index = cloneDanhSachSinhVien.findIndex((item) => {
        return item.maSv == payload;
      });
      cloneDanhSachSinhVien.splice(index, 1);
      state.danhSachSinhVien = cloneDanhSachSinhVien;
      state.seachList = [];
      return { ...state };
    }
    case EDIT: {
      state.editSv = payload;
      state.infor = state.editSv;
      state.isDisabled = true;
      return { ...state };
    }
    case CHANGE_SEARCH_FORM: {
      let value = payload.target.value;
      state.search = value;
      return { ...state };
    }
    case SEARCH: {
      let filterList = state.danhSachSinhVien.filter((item) => {
        return item.ten == state.search;
      });
      state.seachList = filterList;
      state.search = "";
      return { ...state };
    }
    default:
      return state;
  }
};
