import { combineReducers } from "redux";
import { formReducers } from "./formReducer";

export const rootReducers = combineReducers({ formReducers });
