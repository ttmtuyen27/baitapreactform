import React, { Component } from "react";
import DanhSachSinhVien from "./DanhSachSinhVien";
import FormSinhVien from "./FormSinhVien";

export default class BaiTapReactForm extends Component {
  render() {
    return (
      <div>
        <FormSinhVien />
        <DanhSachSinhVien />
      </div>
    );
  }
}
