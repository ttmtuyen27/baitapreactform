import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { CHANGE_FORM, SUBMIT } from "./redux/constants/constants";

class FormSinhVien extends PureComponent {
  render() {
    return (
      <div className="container text-left my-5">
        <h4
          className="h-full bg-dark p-3 mb-2 text-white"
          style={{ fontWeight: "bold" }}
        >
          Thông tin sinh viên
        </h4>
        <div className="row">
          <div className="form-group col-6">
            <label htmlFor>Mã sinh viên</label>
            <input
              type="text"
              className="form-control"
              name="maSv"
              placeholder="Mã sinh viên"
              value={this.props.infor.maSv}
              disabled={this.props.isDisabled}
              onChange={(e) => {
                this.props.handleChangeForm(e);
              }}
            />
            <p id="tbMaSv" className="text-danger display-none"></p>
          </div>
          <div className="form-group col-6">
            <label htmlFor>Tên sinh viên</label>
            <input
              type="text"
              className="form-control"
              name="ten"
              placeholder="Tên sinh viên"
              value={this.props.infor.ten}
              onChange={(e) => {
                this.props.handleChangeForm(e);
              }}
            />
            <p id="tbTen" className="text-danger display-none"></p>
          </div>
          <div className="form-group col-6">
            <label htmlFor>Số điện thoại</label>
            <input
              type="text"
              className="form-control"
              name="sdt"
              placeholder="Số điện thoại"
              value={this.props.infor.sdt}
              onChange={(e) => {
                this.props.handleChangeForm(e);
              }}
            />
            <p id="tbSdt" className="text-danger display-none"></p>
          </div>
          <div className="form-group col-6">
            <label htmlFor>Email</label>
            <input
              type="text"
              className="form-control"
              name="email"
              placeholder="Email"
              value={this.props.infor.email}
              onChange={(e) => {
                this.props.handleChangeForm(e);
              }}
            />
            <p id="tbEmail" className="text-danger display-none"></p>
          </div>
        </div>
        <button className="btn btn-success" onClick={this.props.handleSubmit}>
          Submit
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    infor: state.formReducers.infor,
    editSv: state.formReducers.editSv,
    isDisabled: state.formReducers.isDisabled,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeForm: (e) => {
      return dispatch({
        type: CHANGE_FORM,
        payload: e,
      });
    },
    handleSubmit: () => {
      return dispatch({
        type: SUBMIT,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormSinhVien);
