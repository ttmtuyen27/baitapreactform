import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import {
  CHANGE_SEARCH_FORM,
  DELETE,
  EDIT,
  SEARCH,
} from "./redux/constants/constants";
class DanhSachSinhVien extends PureComponent {
  render() {
    return (
      <div className="container">
        {this.props.danhSachSinhVien.length > 0 && (
          <>
            <div className="form-group text-right">
              <div>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Tìm kiếm"
                  value={this.props.search}
                  onChange={(e) => {
                    this.props.handleSearchForm(e);
                  }}
                  style={{ display: "inline", width: "25%" }}
                />
                <i
                  className="fa fa-search ml-2"
                  style={{ cursor: "pointer" }}
                  onClick={this.props.handleSearchSv}
                />
              </div>
            </div>
            <table className="table">
              <thead className="thead-dark">
                <tr>
                  <th>Mã SV</th>
                  <th>Họ tên</th>
                  <th>Số điện thoại</th>
                  <th>Email</th>
                  <th>Thao tác</th>
                </tr>
              </thead>
              <tbody>
                {this.props.seachList.length <= 0
                  ? this.props.danhSachSinhVien.map((item) => {
                      return (
                        <tr>
                          <td>{item.maSv}</td>
                          <td>{item.ten}</td>
                          <td>{item.sdt}</td>
                          <td>{item.email}</td>
                          <td>
                            <button
                              className="btn btn-info"
                              onClick={() => {
                                this.props.handleEditSv(item);
                              }}
                            >
                              Sửa
                            </button>
                            <button
                              className="btn btn-danger ml-2"
                              onClick={() => {
                                this.props.handleDeleteSv(item.maSv);
                              }}
                            >
                              Xóa
                            </button>
                          </td>
                        </tr>
                      );
                    })
                  : this.props.seachList.map((item) => {
                      return (
                        <tr>
                          <td>{item.maSv}</td>
                          <td>{item.ten}</td>
                          <td>{item.sdt}</td>
                          <td>{item.email}</td>
                          <td>
                            <button
                              className="btn btn-info"
                              onClick={() => {
                                this.props.handleEditSv(item);
                              }}
                            >
                              Sửa
                            </button>
                            <button
                              className="btn btn-danger ml-2"
                              onClick={() => {
                                this.props.handleDeleteSv(item.maSv);
                              }}
                            >
                              Xóa
                            </button>
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachSinhVien: state.formReducers.danhSachSinhVien,
    seachList: state.formReducers.seachList,
    search: state.formReducers.search,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteSv: (maSv) => {
      return dispatch({
        type: DELETE,
        payload: maSv,
      });
    },
    handleEditSv: (sv) => {
      return dispatch({
        type: EDIT,
        payload: sv,
      });
    },
    handleSearchForm: (e) => {
      return dispatch({
        type: CHANGE_SEARCH_FORM,
        payload: e,
      });
    },
    handleSearchSv: () => {
      return dispatch({
        type: SEARCH,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DanhSachSinhVien);
